<?php

/**
 * Creates a link to the given nid with text as the given text.
 *
 * @return
 *    A string of a link.
 */
function pedigree_make_link($nid, $text) {
  $output = l($text, "node/$nid");
  return $output;
}

/**
 * Creates a name out of the given nid, using the first, middle if set, and last names.
 *
 * @return
 *    The newly created name string
 */
function pedigree_make_name($nid, $link = TRUE, $extra = TRUE, $dob = FALSE) {
  $currentnid = 0;
  if ($node = menu_get_object()) {
    // Get the nid
    $currentnid = $node->nid;
  }
  if ($nid) {
    $node = node_load($nid);

    $givenname = entity_metadata_wrapper('node', $node)->pedigree_given_name->value();
    $extraname = entity_metadata_wrapper('node', $node)->pedigree_given_name2->value();
    $familyname = entity_metadata_wrapper('node', $node)->pedigree_family_name->value();

    if (empty($familyname)) {
      $surname = t("No data");
    }
    if (empty($givenname)) {
      $givenname = t("No data");
    }

    $name = "$givenname $extraname $familyname";
    if ($extra == FALSE) {
      $name = "$givenname $familyname";
    }

    if ($link == TRUE && $nid != $currentnid) {
      $name = pedigree_make_link($nid, $name);
    }
    if ($dob == TRUE && entity_metadata_wrapper('node', $node)->pedigree_date_birth->value()) {
      $name = $name . ' (*' . format_date(entity_metadata_wrapper('node', $node)->pedigree_date_birth->value(), 'custom', 'Y') . ')';
    }
    return $name;
  }
  else {
    return t('No data');
  }
}

/**
 * Check and format date for family tree view
 *
 * @return
 * valid date
 */
function pedigree_tree_card_date($value, $bd = '') {
  if ($value == '' || $value == '0000-00-00 00:00:00') {
    $value = '';
  }
  else {
    $value = format_date(strtotime($value), 'custom', $bd . 'Y, j M');
  }
  return $value;
}

/**
 * Calculate age
 */
function pedigree_calculate_age($born, $died) {
  $born = new DateTime($born);
  $died = new DateTime($died);
  $diff = $died->diff($born);
  if ($diff->y == 0) { // less than 1 year old
    if ($diff->m == 0) { // less than 1 month old
      return $diff->d . ' ' . t('days');
    }
    else {
      return $diff->m . ' ' . t('months') . ' ' . $diff->d . ' ' . t('days');
    }
  }
  else {
    return $diff->y . ' ' . t('years');
  }
}

/**
 * Sort an array of nids on a data field
 *
 * @params 
 *    $nids: An array of nids
 *    $field: The field used for sorting
 *    $sort: 'ASC' or 'DESC'
 *
 * @return
 *    An array of sorted nodes
 *
 */
function pedigree_sort_nodes($nids, $field, $sort = 'ASC') {
  $table = 'field_data_' . $field;
  $column = $field . '_value';

  $sortednids = $nids;
  if (count($sortednids) > 0) {
    $sortednids = db_query("SELECT entity_id FROM {$table} WHERE entity_id IN (:nids) ORDER BY $column $sort", array(':nids' => $nids))->fetchCol();  
    // If the field does not exist (empty) for a nid, the nid is not returned.
    // Push back nids that had empty value
    $sortednids = array_merge($sortednids, array_diff($nids, $sortednids));
  }

  return $sortednids;
}

/**
 * Find the partners of a person
 * 
 * @params $pnid
 *    The node id of a person
 *
 * @return
 *    An array of node ids
 *
 */
function pedigree_find_partners($pnid) {
  $partners = array();

  if (isset($pnid)) {
    $prels = db_query("SELECT entity_id FROM {field_data_endpoints} WHERE endpoints_entity_id = :pnid AND endpoints_entity_type = 'node' AND bundle = 'pedigree_partner'", array(':pnid' => $pnid))->fetchCol();
    if (count($prels) > 0) {
      while ($prel = array_pop($prels)) {
	$partners[] = db_query("SELECT endpoints_entity_id FROM {field_data_endpoints} WHERE entity_id = :prel AND endpoints_entity_type = 'node' AND bundle = 'pedigree_partner' AND endpoints_entity_id != :pnid", array(':prel' => $prel, ':pnid' => $pnid))->fetchField();
      }
    }
  }
 
  return $partners;
}

/**
 * Find the parents of a person
 * 
 * @params $pnid
 *    The node id of a person
 *
 * @return
 *    An array of node ids
 *
 */
function pedigree_find_parents($pnid) {
  $parents = array();

  if (isset($pnid)) {
    $rel = db_query("SELECT entity_id FROM {field_data_endpoints} WHERE endpoints_entity_id = :pnid AND endpoints_entity_type = 'node' AND bundle = 'pedigree_child'", array(':pnid' => $pnid))->fetchField();
    if (count($rel) > 0) {
      $prid = db_query("SELECT endpoints_entity_id FROM {field_data_endpoints} WHERE entity_id = :rel AND endpoints_entity_type = 'relation' AND bundle = 'pedigree_child'", array(':rel' => $rel))->fetchField();
      if (isset($prid)) {
	$parents = db_query("SELECT endpoints_entity_id FROM {field_data_endpoints} WHERE entity_id = :parentsrid AND endpoints_entity_type = 'node' AND bundle = 'pedigree_partner'", array(':parentsrid' => $prid))->fetchCol();
      }
    }
  }

  return $parents;
}

/**
 * Find the children of a person
 * 
 * @params $pnid
 *    The node id of a person
 *
 * @return
 *    An array of node ids
 *
 */
function pedigree_find_children($pnid) {
  $children = array();

  $prels = db_query("SELECT entity_id FROM {field_data_endpoints} WHERE endpoints_entity_id = :pnid AND endpoints_entity_type = 'node' AND bundle = 'pedigree_partner'", array(':pnid' => $pnid))->fetchCol();
  if (count($prels) > 0) {
    while ($prel = array_pop($prels)) {
      $crels = db_query("SELECT entity_id FROM {field_data_endpoints} WHERE endpoints_entity_id = :prel AND endpoints_entity_type = 'relation' AND bundle = 'pedigree_child'", array(':prel' => $prel))->fetchCol();
      while ($crel = array_pop($crels)) {
	$children[] = db_query("SELECT endpoints_entity_id FROM {field_data_endpoints} WHERE entity_id = :crel AND endpoints_entity_type = 'node' AND bundle = 'pedigree_child'", array(':crel' => $crel))->fetchField();
      }
    }
  }

  return $children;
}

/**
 * Find the sibling of a person
 * 
 * @params $pnid
 *    The node id of a person
 *
 * @return
 *    An array of node ids
 *
 */
function pedigree_find_siblings($pnid) {
  $siblings = array();

  // get child relation id
  $rel = db_query("SELECT entity_id FROM {field_data_endpoints} WHERE endpoints_entity_id = :pnid AND endpoints_entity_type = 'node' AND bundle = 'pedigree_child'", array(':pnid' => $pnid))->fetchField();
  if (isset($rel)) {
    // get relation id of the parents
    $prid = db_query("SELECT endpoints_entity_id FROM {field_data_endpoints} WHERE entity_id = :rel AND endpoints_entity_type = 'relation' AND bundle = 'pedigree_child'", array(':rel' => $rel))->fetchField();
    if (isset($prid)) {
      // get the children of the parent
      $crels = db_query("SELECT entity_id FROM {field_data_endpoints} WHERE endpoints_entity_id = :parentsrid AND endpoints_entity_type = 'relation' AND bundle = 'pedigree_child'", array(':parentsrid' => $prid))->fetchCol();
      while ($crel = array_pop($crels)) {
	$siblings[] = db_query("SELECT endpoints_entity_id FROM {field_data_endpoints} WHERE entity_id = :crel AND endpoints_entity_type = 'node' AND bundle = 'pedigree_child'", array(':crel' => $crel))->fetchField();
      }
    }
  }

  // remove current person from list
  $siblings = array_diff($siblings, array($pnid));

  return $siblings;
}

<?php
// $Id$

global $called;
$called = 0;

/**
 * Retrieves the information about the given NID's family tree
 *
 * @param $nid
 *    The NID to retrieve family tree information for
 *
 * @return
 *    An associative array containing the family tree information for the individual NID:
 *    - nid: The NID the information belongs to
 *    - name: The name of the person
 *    - birth: The birthdate of the person
 *    - death: The deathdate of the person
 *    - deathplace: The death place of the person
 *    - gender: The gender of the person
 */
function pedigree_tree_get_info($nid) {
  $output = array();

  if ($nid > 0) {
    $node = node_load($nid);

    $output['nid'] = $nid;
    $output['gender'] = entity_metadata_wrapper('node', $node)->pedigree_gender->value();
    $output['name'] = entity_metadata_wrapper('node', $node)->pedigree_given_name->value() . ' ' . entity_metadata_wrapper('node', $node)->pedigree_family_name->value();
    $birth = field_get_items('node', $node, 'pedigree_date_birth');
    $output['birth'] = $birth[0]['value'];
    $death = field_get_items('node', $node, 'pedigree_date_death');
    $output['death'] = $death[0]['value'];
    $output['deathplace'] = entity_metadata_wrapper('node', $node)->pedigree_place_death->value();
  }

  return $output;
}

/**
 * Retrievs header information for the edit ascendents page
 *
 * @param $nid
 *    The NID of the node being edited
 * @param $length
 *    The generation this person belongs to(?)
 * @param $detail 
 *    Any details to be added
 *
 * @return
 *    A string of the tree's content.
 */
function pedigree_tree_header($nid, $length = 2, $detail = null) {
  if (!isset($detail)) {
    $detail = 2;
  }
  $treecontent = '<p>' . t('Choose number of generations to show and level of detail.') . '</p>';
  $treecontent .= '<form method="GET" action="#">';
  $treecontent .= t('Generations: ');
  $treecontent .= '<select name="len">';
  for ($i = 1; $i <= 4; $i++) {
    $treecontent .= '<option value="' . $i . '"';
    if ($i == $length) {
      $treecontent .= ' SELECTED';
    }
    $treecontent .= '>' . ($i + 1) . '</option>';
  }
  $treecontent .= '</select> ';
  $treecontent .= t('Level of Detail: ');
  $treecontent .= '<select name="det">';
  for ($i = 1; $i <= 2; $i++) {
    $treecontent .= '<option value="' . $i . '"';
    if ($i == $detail) {
      $treecontent .= ' SELECTED';
    }
    $treecontent .= '>' . $i . '</option>';
  }
  $treecontent .= '</select> <input type="submit" value="Submit"></form>';

  return $treecontent;
}


/**
 * Format image for tree element
 *
 *
 *
 */
function pedigree_tree_image($tree, $lev, $fam) {
  global $base_url;
  $path = $base_url . '/' . drupal_get_path('module', 'pedigree');
//  $cellwidth = 160;
//  $padd = 5;
  $style = '" style="display: inline; position: relative; top: 0px; height: 10px; width: 100%; padding: 0; margin: 0;"';
//  $style = '" style="position: relative; top: 0; display: inline; height: 10px; width: ' . $cellwidth . 'px; padding: 0; margin: 0;"';
  $lfill = ''; //'<img src="' . $path . '/img/hfill.png" class="lfill" style="position: relative; top: 10px; left: 0; display: inline; width: 50%; height: 10px; padding: 0; margin: 0;" />';
  $rfill = ''; //'<img src="' . $path . '/img/hfill.png" class="rfill" style="position: relative; top: 0px; float: right; display: inline; width: 50%; height: 10px; padding: 0; margin: 0;" />';
  $hfill = ''; //'<img src="' . $path . '/img/hfill.png" style="position: relative; top: -10px; left: 0; display: inline; width: ' . $padd . 'px; height: 10px; padding: 0; margin: 0;" />';
//  if (isset($tree[$lev + 1][0])) {
//    $style = '" style="width:'  . ($tree[$lev + 1][0] * ($cellwidth + (2 *$padd))) . 'px; height: 10px; width: 100%; padding: 0; margin: 0;"';
//  }
  $image = '';

  // determine which image to place above descendant
  if (isset($tree[$lev][$fam + 1]) && ($tree[$lev][$fam - 1]['parent'] == $tree[$lev][$fam + 1]['parent'])) {
    $image .= $lfill . '<img src="' . $path . '/img/center.png"' . $style . '/>' . $rfill;
  }
  elseif (isset($tree[$lev][$fam - 1]) && ($tree[$lev][$fam]['parent'] == $tree[$lev][$fam - 1]['parent'])) {
    $image .= $lfill . '<img src="' . $path . '/img/right.png"' . $style . '/>';
  }
  elseif (isset($tree[$lev][$fam + 1]) && ($tree[$lev][$fam]['parent'] == $tree[$lev][$fam + 1]['parent'])) {
    $image .= '<img src="' . $path . '/img/left.png"' . $style . '/>' . $rfill;
  }
  else { 
    $image .= '<img src="' . $path . '/img/single.png"' . $style . '/>';
  }
  return $image;
}

/**
 * Format name for tree element
 *
 *
 *
 */
function pedigree_tree_name($tree, $lev, $fam, $det, $asc = 'desc', $more = 'more' ) {
  $card = '';

  $cellwidth = variable_get('pedigree_card_width', 160);
  $cellheight = variable_get('pedigree_card_height', 220);
  $padd = variable_get('pedigree_card_padding', 5);
  $border = 2; // variable_get('pedigree_card_border_width', 2);

  $age = '';
  $gender = '';
  if (!isset($det)) {
    $det = 2;
  }

  // set name and dates
  if (isset($tree[$lev][$fam]['gender'])) {
    if ($tree[$lev][$fam]['gender'] == '1') {
      $gender = t('♀');
    }
    elseif ($tree[$lev][$fam]['gender'] == '2') {
      $gender = t('♂');
    }
    elseif ($tree[$lev][$fam]['gender'] == '0') {
      $gender = t('unknown');
    }
    else {
      $gender = t('unknown');
    }
  }
  $card .= '<div class="fam_card" style="width: ' . ($cellwidth - (2 * $border) - (2 * $padd)) . 'px; height: ' . $cellheight . 'px; padding: 0 ' . $padd . 'px; margin-left: auto; margin-right: auto;">';
  $card .= '<div style="text-align: center;"><a href="/node/' . $tree[$lev][$fam]['nid'] . '/tree/ascendants" title="' . t('View ancestors') . '">▲</a></div>';
  if (!isset($tree[$lev][$fam]['blank']) || $tree[$lev][$fam]['blank'] == FALSE) { 
    $card .=  '<strong>' . pedigree_make_name($tree[$lev][$fam]['nid'], TRUE, FALSE, FALSE) . " $gender</strong>";
    if ($det == 2 && $tree[$lev][$fam]['name'] != 'Private') {
      $age = '';
      if ($tree[$lev][$fam]['birth'] != '') {
	$age = pedigree_calculate_age($tree[$lev][$fam]['birth'],$tree[$lev][$fam]['death']);
	$card .= ' <br/>' . pedigree_tree_card_date($tree[$lev][$fam]['birth'], '*');
      }
      if ($tree[$lev][$fam]['deathplace'] != '') {
	$age = '';
      }
      if ($tree[$lev][$fam]['death'] != '') {
	if ($tree[$lev][$fam]['birth'] != '') {
	  $age = pedigree_calculate_age($tree[$lev][$fam]['birth'],$tree[$lev][$fam]['death']);
	}
	$card .= '<br/>' . pedigree_tree_card_date($tree[$lev][$fam]['death'], '+');
      }
      if ($age != '') {
	$card .= '<br/>' . $age;
      }
    }
  }
  else {
    $card .= '&nbsp;';
  }
  if ($asc != 'asc') {
    $partners = pedigree_find_partners($tree[$lev][$fam]['nid']);
    /*
    // Sort partners on day of marriage TODO: Does this not work,
    // because this is a relation field
    if (count($partners) > 1) {
      $partners = pedigree_sort_nodes($partners, 'pedigree_date_marr_rel', 'DESC');
    }
     */
    if (count($partners) == 1) {
      $card .= '<br/><span style="font-size: large;">' . t('×') . '</span>'; // ⚭ 
    }
    if (count($partners) > 1) {
      $card .= '<br/><span style="font-size: large;">' . t('××') . '</span>';
    }
    foreach ($partners as $partner) {
      $card .= '<br/>' . pedigree_make_name($partner, TRUE, FALSE, FALSE);
    }
  }
  $card .= '<div style="text-align: center;"><a href="/node/' . $tree[$lev][$fam]['nid'] . '/tree/descendants" title="' . t('View descendants') . '">▼</a></div>';
  $card .= '</div>';
  return $card;
}

/**
 * Helper function to get the ascendents of a given node
 *
 * @param $nid
 *    The NID of the node being edited
 *
 * @return
 *    String of HTML contain the node's ascendents and display information
 */
function pedigree_view_tree_asc($nid = 0) {
  $cellwidth = variable_get('pedigree_card_width', 160);
  $cellheight = variable_get('pedigree_card_height', 220);
  $padd = variable_get('pedigree_card_padding', 5);
  $border = 2; // variable_get('pedigree_card_border_width', 2);

  if (isset($_GET['len'])) {
    $length = $_GET['len'];
  }
  if (isset($_GET['det'])) {
    $detail = $_GET['det'];
  }
  else {
    $detail = null;
  }
  if (!isset($length)) {
    $length = 2;
  }

  $treecontent = '';

  // Insert form with tree display options
  $treecontent .= pedigree_tree_header($nid, $length, $detail);

  //Get Data
  $treearray[0][1] = pedigree_tree_get_info($nid);

  for ($level = 1; $level <= $length; $level++) {
    for ($familyno = 1; $familyno <= pow(2, $level); $familyno++) {
      if (isset($treearray[($level - 1)][(ceil($familyno / 2))])) {
	$cnid = $treearray[($level - 1)][(ceil($familyno / 2))]['nid'];
	$parents = pedigree_find_parents($cnid);
	if (count($parents) > 0) {
	  foreach ($parents as $pnid) {
	    $treearray[$level][$familyno] = pedigree_tree_get_info($pnid);
	    $familyno++;
	  }
	  $familyno--;
	}
      }
    }
  }

  //Output data
  global $base_url;
  $treecontent .= t('You may need to scroll horizontaly and/or vertically to see all tree content.');
  $treecontent .= '<div class="fam_tree" style="max-width: 100%; width: ' . (pow(2, $length) * ($cellwidth + (2 * $border) + (2 * $padd))) . 'px; overflow: auto; margin-left: auto; margin-right: auto;">';

  for ($level = $length; $level >= 0; $level--) {
    $treecontent .= '<div style="margin-left: auto; margin-right: auto;">';
    for ($familyno = 1; $familyno <= pow(2, $level); $familyno++) {
      $class = 'fam_levelfive';
      if ($level == 3) {
        $class = 'fam_levelfour';
      }
      elseif ($level == 2) {
        $class = 'fam_levelthree';
      }
      elseif ($level == 1) {
        $class = 'fam_leveltwo';
      }
      elseif ($level == 0) {
        $class = 'fam_levelone';
      }

      $treecontent .= '<div class="' . $class . '" style="width: auto; margin-left: auto; margin-right: auto; display: table-cell;">';
      if ($level != $length) {
	$treecontent .= ' <img src="' . $base_url . '/' . drupal_get_path('module', 'pedigree') . '/img/asctree.png' .  '" style="width:' . (pow(2, $length) * $cellwidth / pow(2, $level)) . 'px; height: 20px"></br>';
      }
      /*      else {
	$treecontent .= '→';
      }
       */
      if (isset($treearray[$level][$familyno]['name'])) {
	$treecontent .= pedigree_tree_name($treearray, $level, $familyno, $detail, 'asc');
      }
      else {
	$treecontent .= '<div class="fam_card" style="width:' . ($cellwidth - (2 * $border) - (2 * $padd)) . 'px; height: ' . $cellheight . 'px; padding: 0 ' . $padd . 'px; margin-left: auto; margin-right: auto;">' . t('Unknown') . '</div>';
      }
      $treecontent .= '</div>';
    }
    $treecontent .= '</div>';
  }
  $treecontent .= '</div>';
  return $treecontent;
}

/**
 * Helper function to get the descendants of a given node
 *
 * @param $nid
 *    The NID of the node being edited
 *
 * @return
 *    String of HTML contain the node's descendants and display information
 */
function pedigree_view_tree_desc($nid = 0) {
  $cellwidth = variable_get('pedigree_card_width', 160);
  $cellheight = variable_get('pedigree_card_height', 220);
  $padd = variable_get('pedigree_card_padding', 5);
  $border = 2; // variable_get('pedigree_card_border_width', 2);

  if (isset($_GET['len'])) {
    $length = $_GET['len'];
  }
  (isset($_GET['det'])) ? $detail = $_GET['det'] : $detail = null;
  if (!isset($length)) {
    $length = 2;
  }
  if (isset($treecontent)) {
    $treecontent .= pedigree_tree_header($nid, $length, $detail);
  }
  else {
    $treecontent = pedigree_tree_header($nid, $length, $detail);
  }

  //Get Data
  $treearray[0][1] = pedigree_tree_get_info($nid);

  $treearray[0][0] = 1;
  for ($level = 1; $level <= $length; $level++) {
    $treearray[$level][0] = 0;
    $familyno = 1; // Start with family number 1 on each level

    // Loop through all parents for a $level
    for ($parentno = 1; $parentno <= $treearray[$level - 1][0]; $parentno++) {

      // Only do something if parent has children ('blank' not set to TRUE)
      if (!isset($treearray[$level - 1][$parentno]['blank']) || $treearray[$level - 1][$parentno]['blank'] == FALSE) {

	// Get the children of the parent
	$children = pedigree_find_children($treearray[$level - 1][$parentno]['nid']);

	if (count($children) > 0) { // If there are children...
	// Sort children on day of birth
	$children = pedigree_sort_nodes($children, 'pedigree_date_birth', 'ASC');
	  foreach ($children as $child) { // ...loop through the children
	    $treearray[$level][0]++;
	    $treearray[$level][$familyno] = pedigree_tree_get_info($child);
	    $treearray[$level][$familyno]['blank'] = FALSE;
	    $treearray[$level][$familyno]['parent'] = $parentno;
	    $familyno++;
	  }
	}
	else { // If there are no children
	  $treearray[$level][0]++;
	  $treearray[$level][$familyno]['blank'] = TRUE;
	  $treearray[$level][$familyno]['parent'] = $parentno;
	  $familyno++;
	}
      }
    }
  }

  //Output Data
  global $base_url;

  /*
  $maxwidth =  $treearray[$length][0];
  if (isset($treearray[$length + 1][0]) && ($treearray[$length + 1][0] > $maxwidth)) {
    $maxwidth =  $treearray[$length + 1][0];
    if (isset($treearray[$length + 2][0]) && ($treearray[$length + 2][0] > $maxwidth)) {
      $maxwidth =  $treearray[$length + 2][0];
    }
  }
   */

  $treecontent .= t('You may need to scroll horizontaly and/or vertically to see all tree content.');
  $treecontent .= '<div class="fam_tree" style="">';
  $treecontent .= '<div style="overflow: auto; margin-left: auto; margin-right: auto;" class="fam_levelone">'; // outermost table, level 1
  // Data for parent of all descendants
  $level = 0;

  //  $treecontent .= '<div style="padding: 0 ' . $padd . 'px; margin-left: auto; margin-right: auto;">';
  // set name and dates of first parent now, but only once.
  // TODO: still has to be centered correctly when table is wide or narrow
  $treecontent .= pedigree_tree_name($treearray, $level, 1, $detail);
  $level++;

  for ($familyno = 1; $familyno <= $treearray[$level][0]; $familyno++) {
    if ((!isset($treearray[$level][$familyno]['blank']) || $treearray[$level][$familyno]['blank'] == FALSE) && $level != 0) {

      $treecontent .= '<div class="fam_leveltwo" style="display: table-cell; margin-left: auto; margin-right: auto;">'; // level 2 table
      // determine which image to place above descendant
      $treecontent .= pedigree_tree_image($treearray, $level, $familyno);
      $treecontent .= '<div style="padding: 0 ' . $padd . 'px;">';

      // set name and dates
      $treecontent .= pedigree_tree_name($treearray, $level, $familyno, $detail);

      if ($length > 1) {
	for ($familynoo = 1; $familynoo <= $treearray[$level + 1][0]; $familynoo++) {
	  if ($treearray[$level + 1][$familynoo]['parent'] == $familyno && (!isset($treearray[$level + 1][$familynoo]['blank']) || $treearray[$level + 1][$familynoo]['blank'] == FALSE) && $level + 1 != 0) {
	    $treecontent .= '<div class="fam_levelthree" style="display: table-cell; margin-left: auto; margin-right: auto;">'; // level 3 table

	    // determine which image to place above descendant
	    $treecontent .= pedigree_tree_image($treearray, $level + 1, $familynoo);
	    $treecontent .= '<div style="padding: 0 ' . $padd . 'px;">';

	    // set name and dates
	    $treecontent .= pedigree_tree_name($treearray, $level + 1, $familynoo, $detail);

	    // level 3
	    if ($length > 2) {
	      for ($familynooo = 1; $familynooo <= $treearray[$level + 2][0]; $familynooo++) {
		if ($treearray[$level + 2][$familynooo]['parent'] == $familynoo && (!isset($treearray[$level + 2][$familynooo]['blank']) || $treearray[$level + 2][$familynooo]['blank'] == FALSE) && $level + 2 != 0) {
		  $treecontent .= '<div class="fam_levelfour" style="display: table-cell; margin-left: auto; margin-right: auto;">'; // level 4 table

		  // determine which image to place above descendant
		  $treecontent .= pedigree_tree_image($treearray, $level + 2, $familynooo);
		  $treecontent .= '<div style="padding: 0 ' . $padd . 'px;">';
		  // set name and dates
		  $treecontent .= pedigree_tree_name($treearray, $level + 2, $familynooo, $detail);

		  // level 4
		  if ($length > 3) {
		    for ($familynoooo = 1; $familynoooo <= $treearray[$level + 3][0]; $familynoooo++) {
		      if ($treearray[$level + 3][$familynoooo]['parent'] == $familynooo && (!isset($treearray[$level + 3][$familynoooo]['blank']) || $treearray[$level + 3][$familynoooo]['blank'] == FALSE) && $level + 3 != 0) {
			$treecontent .= '<div class="fam_levelfive" style="display: table-cell; margin-left: auto; margin-right: auto;">'; // level 4 table

			// determine which image to place above descendant
			$treecontent .= pedigree_tree_image($treearray, $level + 3, $familynoooo);
			$treecontent .= '<div style="padding: 0 ' . $padd . 'px;">';
			// set name and dates
			$treecontent .= pedigree_tree_name($treearray, $level + 3, $familynoooo, $detail);
			$treecontent .= '</div>';
			$treecontent .= '</div>'; // close level 4 table
		      }
		    }
		  }
		  $treecontent .= '</div>';
		  $treecontent .= '</div>'; // close level 4 table
		}
	      }
	    }
	    $treecontent .= '</div>';
	    $treecontent .= '</div>'; // close level 3 table
	  }
	}
      }
      $treecontent .= '</div>';
      $treecontent .= '</div>'; // close level 2 table
    }
  }
$treecontent .= '</div>'; // close level 1 table

return $treecontent;
}
